class Employee:
    # method initializer method with required state
    def __init__(self, first_name: str, last_name: str) -> None:
        self.first_name = first_name
        self.last_name = last_name

    def get_fullname(self) -> str:
        return f"{self.first_name} {self.last_name}"

    def get_email(self) -> str:
        return f"{self.first_name.lower()}.{self.last_name.lower()}@company.com"


# Tests
employee = Employee("Duska", "Ruzicka")
print(employee.get_fullname())  # prints "Duska Ruzicka"
print(employee.get_email())  # prints "duska.ruzicka@company.com"
