def can_skydive(age: int, has_consent_form: bool):
    # If the age is greater than or equal to 18 or they
    # have a consent form
    return age >= 18 or has_consent_form
