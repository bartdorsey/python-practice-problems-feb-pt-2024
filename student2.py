class Student:
    def __init__(self, name: str) -> None:
        self.name = name
        self.scores: list[int] = []

    def add_score(self, score: int) -> None:
        self.scores.append(score)

    def get_average(self) -> float | None:
        num_scores = len(self.scores)
        if num_scores == 0:
            return
        # Average - sum / number of scores
        sum = 0
        for score in self.scores:
            sum += score
        return sum / num_scores


# Tests

student = Student("Malik")
print(student.get_average())  # Prints None
student.add_score(80)
print(student.get_average())  # Prints 80
student.add_score(90)
student.add_score(82)
print(student.get_average())  # Prints 84
