def max_of_three(value1: int, value2: int, value3: int):
    # If value1 is greater than or equal to value2
    # and value1 is greater than or equal to value3
    if value1 >= value2 and value1 >= value3:
        # Return value1
        return value1
    # Otherwise, if value2 is greater than or equal to value1
    # and value2 is greater than or equal to value3
    if value2 >= value1 and value2 >= value3:
        # Return value2
        return value2
    # Otherwise,
    # Return value3
    return value3


print(max_of_three(1, 2, 3))
print(max_of_three(1, 2, 2))
print(max_of_three(3, 2, 1))
print(max_of_three(3, 3, 3))
