def is_palindrome(word: str):
    # Reverse the word into a list of letters
    # "hello" becomes ["o", "l", "l", "e", "h"]
    reversed_list_of_letters = reversed(word)

    # Join the letters together using the empty string
    # ["o", "l", "l", "e", "h"] becomes "olleh"
    reversed_word = "".join(reversed_list_of_letters)

    # If reversed_word equals word
    return reversed_word == word
