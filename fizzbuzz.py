def fizzbuzz(number: int) -> int | str:
    # This uses the fancy new match keyword in python 3.10
    match number % 3, number % 5:
        case 0, 0:
            return "fizzbuzz"
        case _, 0:
            return "fizz"
        case 0, _:
            return "buzz"
        case _:
            return number


print(fizzbuzz(6))
print(fizzbuzz(30))
print(fizzbuzz(10))
print(fizzbuzz(19))
