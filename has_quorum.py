def has_quorum(attendees_list: list[str], members_list: list[str]):
    return len(attendees_list) >= len(members_list) / 2


print(has_quorum(["Noor"], ["Basia", "Noor", "Laila", "Talia", "Zahara"]))
print(has_quorum(["Basia", "Noor"], ["Basia", "Noor", "Laila", "Talia", "Zahara"]))
print(has_quorum(["Basia", "Laila"], ["Basia", "Noor", "Laila", "Talia"]))
print(has_quorum([], ["Basia", "Noor", "Laila", "Talia", "Zahara"]))
print(
    has_quorum(
        ["Noor", "Laila", "Zahara"], ["Basia", "Noor", "Laila", "Talia", "Zahara"]
    )
)
print(
    has_quorum(
        ["Basia", "Noor", "Laila", "Zahara"],
        ["Basia", "Noor", "Laila", "Talia", "Zahara"],
    )
)
print(
    has_quorum(
        ["Basia", "Noor", "Laila", "Talia", "Zahara"],
        ["Basia", "Noor", "Laila", "Talia", "Zahara"],
    )
)
