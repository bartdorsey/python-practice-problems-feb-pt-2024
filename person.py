from typing import Optional


class Person:
    def __init__(self, name: str, hated_foods: list[str], loved_foods: list[str]):
        self.name = name
        self.hated_foods = hated_foods
        self.loved_foods = loved_foods

    def taste(self, potato: str) -> Optional[bool]:
        # returns True if the food name is in their loved food list
        if potato in self.loved_foods:
            return True
        # returns False if the food name is in their hated food list
        if potato in self.hated_foods:
            return False


# Test
person = Person("Malik", ["cottage cheese", "sauerkraut"], ["pizza", "schnitzel"])
print(person.taste("lasagna"))  # Prints None, not in either list
print(person.taste("sauerkraut"))  # Prints False, in the hated list
print(person.taste("pizza"))  # Prints True, in the loved list
