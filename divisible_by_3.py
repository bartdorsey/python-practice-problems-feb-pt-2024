from typing import Literal


def divisible_by_3(number: int) -> int | Literal["fizz"]:
    match number % 3:
        case 0:
            return "fizz"
        case _:
            return number
