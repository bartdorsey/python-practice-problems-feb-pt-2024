def mode(numbers: list[int]) -> list[int]:
    frequency = {}
    for num in numbers:
        if frequency.get(num):
            frequency[num] += 1
        else:
            frequency[num] = 1
    modes = []
    maximum = max(frequency.values())
    for k, v in frequency.items():
        if v == maximum:
            modes.append(k)
    return modes


print(mode([1, 2, 3, 3, 4, 5, 5]))
