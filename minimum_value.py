def minimum_value(value1: int, value2: int) -> int:
    if value1 < value2:
        return value1  # Early Return
    return value2


print(f"{minimum_value(1, 3)=}")
print(f"{minimum_value(3, 2)=}")
print(f"{minimum_value(3, 3)=}")
