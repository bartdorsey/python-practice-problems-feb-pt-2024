from typing import Literal


def divisible_by_5(number: int) -> int | Literal["buzz"]:
    match number % 5:
        case 0:
            return "buzz"
        case _:
            return number
